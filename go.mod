module github.com/go-kratos/kratos-layout

go 1.20

require (
	github.com/fatih/structs latest
	github.com/go-kratos/kratos/v2 latest
	github.com/google/wire latest
	github.com/gorilla/handlers latest
	go.uber.org/automaxprocs latest
	google.golang.org/protobuf latest
	gorm.io/gorm latest
)
