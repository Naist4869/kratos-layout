//go:build wireinject
// +build wireinject

// The build tag makes sure the stub is not built in the final build.

package data

import (
	"github.com/go-kratos/kratos-layout/internal/conf"

	"github.com/go-kratos/kratos/v2/log"
	"github.com/google/wire"
)

func newTestData(*conf.Data, log.Logger) (*Data, func(), error) {
	panic(wire.Build(DataProviderSet))
}
