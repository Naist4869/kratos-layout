package data

import (
	"github.com/go-kratos/kratos-layout/internal/biz"
	"github.com/go-kratos/kratos-layout/internal/conf"
	"github.com/go-kratos/kratos/v2/config"
	"github.com/go-kratos/kratos/v2/log"
	"os"
	"testing"
)

var (
	testData *Data
	testRepo biz.GreeterRepo
	ctx      = context.Background()
)

func loadConfig() (*conf.Bootstrap, *conf.Registry) {
	source, _ := bootstrap.NewFileConfigSource("../../configs")
	c := config.New(
		config.WithSource(source),
	)
	if err := c.Load(); err != nil {
		panic(err)
	}

	var bc conf.Bootstrap
	if err := c.Scan(&bc); err != nil {
		panic(err)
	}

	var rc conf.Registry
	if err := c.Scan(&rc); err != nil {
		panic(err)
	}

	return &bc, &rc
}

func TestMain(m *testing.M) {
	stdLogger := log.NewStdLogger(os.Stdout)

	config, _ := loadConfig()
	var (
		f   func()
		err error
	)
	testData, f, err = newTestData(config.Data, stdLogger)
	defer f()
	if err != nil {
		panic("初始化DATA失败")
	}

	testRepo = NewGreeterRepo(testData, stdLogger)
	os.Exit(m.Run())
}
