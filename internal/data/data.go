package data

import (
	"github.com/go-kratos/kratos-layout/internal/conf"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/google/wire"
	"gorm.io/gorm"
)

// ProviderSet is data providers.
var ProviderSet = wire.NewSet(
	NewGreeterRepo,
	DataProviderSet,
)

var DataProviderSet = wire.NewSet(
	wire.FieldsOf(new(*conf.Data), "Database"),
	ConvertMysqlConfig,
	mysql.New,
	NewData,
)

// Data .
type Data struct {
	*gorm.DB
	logger *log.Helper
}

// NewData .
func NewData(c *conf.Data, db *gorm.DB, logger log.Logger) (*Data, func(), error) {
	l := log.NewHelper(log.With(logger, "data", "data"))
	cleanup := func() {
		l.Info("closing the data resources")
	}

	return &Data{
		DB:     db,
		logger: l,
	}, cleanup, nil
}
