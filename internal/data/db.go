package data

import (
	"github.com/go-kratos/kratos-layout/internal/conf"
)

func ConvertMysqlConfig(conf *conf.Data_Database) *mysql.Config {
	return &mysql.Config{
		Host:            conf.Host,
		Username:        conf.Username,
		Password:        conf.Password,
		DbName:          conf.DbName,
		Port:            conf.Port,
		MaxIdleConns:    int(conf.MaxIdleConns),
		MaxOpenConns:    int(conf.MaxOpenConns),
		ConnMaxLifetime: conf.ConnMaxLifetime.AsDuration(),
	}
}
