package server

import (
	"github.com/google/wire"
)

// ProviderSet is server providers.
var ProviderSet = wire.NewSet(
	wire.FieldsOf(new(bootstrap.Polaris), "Registrar", "Discovery"), NewHTTPServer, NewGRPCServer)
