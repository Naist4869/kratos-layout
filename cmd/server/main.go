package main

import (
	"flag"
	"github.com/fatih/structs"
	"github.com/go-kratos/kratos-layout/internal/conf"
	"github.com/go-kratos/kratos/v2"
	"github.com/go-kratos/kratos/v2/config"
	"time"

	"github.com/go-kratos/kratos/v2/log"
	"github.com/go-kratos/kratos/v2/registry"
	"github.com/go-kratos/kratos/v2/transport/grpc"
	"github.com/go-kratos/kratos/v2/transport/http"
	_ "go.uber.org/automaxprocs"
)

// go build -ldflags "-X main.Version=x.y.z"
var (
	Name    string
	Version string
	Service = bootstrap.NewServiceInfo(
		Name,
		Version,
		"",
	)
	Flags = bootstrap.NewCommandFlags()
)

func init() {
	Flags.Init()
}

func newApp(logger log.Logger, r registry.Registrar, hs *http.Server, gs *grpc.Server) *kratos.App {
	return kratos.New(
		kratos.ID(Service.GetInstanceId()),
		kratos.Name(Service.Name),
		kratos.Version(Service.Version),
		kratos.Metadata(Service.Metadata),
		kratos.Logger(logger),
		kratos.Server(
			hs,
			gs,
		),
		kratos.Registrar(r),
	)
}
func loadConfig() (bootstrap.Polaris, *conf.Bootstrap, *conf.Registry) {
	polaris, err := bootstrap.NewPolaris(Flags.ConfigHost, Service.Name, Flags.Env)
	if err != nil {
		panic(err)
	}
	sources := []config.Source{
		polaris.Config,
	}
	source, exist := bootstrap.NewFileConfigSource(Flags.Conf)
	if exist {
		sources = append(sources, source)
	}
	c := config.New(
		config.WithSource(
			sources...,
		),
	)

	if err := c.Load(); err != nil {
		panic(err)
	}

	var bc conf.Bootstrap
	if err := c.Scan(&bc); err != nil {
		panic(err)
	}

	var rc conf.Registry
	if err := c.Scan(&rc); err != nil {
		panic(err)
	}

	return polaris, &bc, &rc
}

func main() {
	flag.Parse()
	loc, err := time.LoadLocation("Asia/Shanghai")
	if err != nil {
		panic(err.Error())
	}
	time.Local = loc
	logger := bootstrap.NewLoggerProvider(&Service)
	structs.DefaultTagName = "json"

	polaris, bc, rc := loadConfig()
	if bc == nil || rc == nil {
		panic("load config failed")
	}
	if bc == nil || rc == nil {
		panic("load config failed")
	}

	err = bootstrap.NewTracerProvider(bc.Trace.Endpoint, Flags.Env, &Service)
	if err != nil {
		panic(err)
	}
	app, cleanup, err := wireApp(polaris, bc.Server, rc, bc.Data, logger)
	if err != nil {
		panic(err)
	}
	defer cleanup()

	// start and wait for stop signal
	if err := app.Run(); err != nil {
		panic(err)
	}
}
